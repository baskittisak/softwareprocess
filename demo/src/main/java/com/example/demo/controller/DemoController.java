/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.controller;

import com.example.demo.model.Demo;
import com.example.demo.model.DemoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Student
 */


@Controller
        
public class DemoController {
    
    @Autowired
    DemoRepository demoRepository;
    
    @GetMapping("/")
    public String demoHome() {
        return "homepage";
    } 
    
    @GetMapping("/demo")
    public Demo getDemo() {
        return demoRepository.getOne(1);
    }
    
}
